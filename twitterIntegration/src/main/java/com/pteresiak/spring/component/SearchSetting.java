package com.pteresiak.spring.component;

import java.text.Normalizer;

import java.util.HashSet;

import java.util.Set;

import org.springframework.stereotype.Repository;

@Repository
public class SearchSetting implements SearchSettingInterf {

	private String channel;
	private Set<String> keyWords;
	private String newKeyWord;
	private boolean isEmptyKeyWords;
	private final String defaultChannelName;
	private int nrPage;
	

	public SearchSetting() {

		this.defaultChannelName = "gildiagraczypl";
		this.channel = defaultChannelName;
		this.keyWords = new HashSet<String>();
		this.isEmptyKeyWords = true;
		this.nrPage=1;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {

		this.channel = Normalizer.normalize(channel, Normalizer.Form.NFD).replaceAll("\\W", "");
	}

	public Set<String> getKeyWords() {
		return keyWords;
	}

	public boolean isEmptyKeyWords() {
		return isEmptyKeyWords;
	}

	public String getNewKeyWord() {
		return newKeyWord;
	}

	public void setNewKeyWord(String newKeyWord) {
		this.newKeyWord = newKeyWord.toUpperCase();
	}

	public String getDefaultChannelName() {
		return defaultChannelName;
	}

	public void setKeyWords(Set<String> keyWords) {
		this.keyWords = keyWords;
		this.isEmptyKeyWords = this.keyWords.isEmpty();
	}

	public int getNrPage() {
		return nrPage;
	}

	public void setNrPage(int nrPage) {
		if (nrPage < 1) {
			this.nrPage = 1;
		} else {
			this.nrPage = nrPage;
		}

	}

	public void setEmptyKeyWords(boolean isEmptyKeyWords) {
		this.isEmptyKeyWords = isEmptyKeyWords;
	}

	public void addKeyWord(String key) {
		this.keyWords.add(key.toUpperCase());
		this.isEmptyKeyWords = this.keyWords.isEmpty();
	}

	public void removeKeyWord(String key) {
		this.keyWords.remove(key.toUpperCase());
		this.isEmptyKeyWords = this.keyWords.isEmpty();
	}

	public void clearKeyWords() {
		this.keyWords.clear();
		this.isEmptyKeyWords = this.keyWords.isEmpty();
	}

	@Override
	public String toString() {
		return "SearchSetting [channel=" + channel + ", keyWords=" + keyWords + ", newKeyWord=" + newKeyWord
				+ ", isEmptyKeyWords=" + isEmptyKeyWords + ", defaultChannelName=" + defaultChannelName + "]";
	}

	@Override
	public String parseKeyWords() {
		StringBuilder parseStringBuilder = new StringBuilder(" ");
		for (String string : keyWords) {
			parseStringBuilder.append(" ");
			parseStringBuilder.append(string);
		}
		parseStringBuilder.append(" ");
		return parseStringBuilder.toString();
	}

	@Override
	public void parseKeyWords(String filtr) {
		String[] filtrerWords = filtr.split(" ");
		this.clearKeyWords();
		for (String string : filtrerWords) {
			if ("".equals(string) || " ".equals(string)) {
				continue;
			}
			this.keyWords.add(string);
		}
		this.isEmptyKeyWords = this.keyWords.isEmpty();

	}

}
