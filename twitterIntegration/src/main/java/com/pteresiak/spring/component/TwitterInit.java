package com.pteresiak.spring.component;

import twitter4j.*;



public class TwitterInit {
	protected Twitter twitter;
	protected TwitterFactory twitterFactory;
	

	public TwitterInit() {
		twitterFactory = new TwitterFactory();
		twitter = this.twitterFactory.getInstance();
	
	}

	public Twitter getTwitter() {
		return twitter;
	}

	public TwitterFactory getTwitterFactory() {
		return twitterFactory;
	}
}
