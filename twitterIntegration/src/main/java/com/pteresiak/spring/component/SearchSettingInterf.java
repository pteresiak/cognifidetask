package com.pteresiak.spring.component;

import java.util.Set;

public interface SearchSettingInterf {
	public String getChannel();

	public void setChannel(String channel);

	public String getNewKeyWord();

	public void setNewKeyWord(String newKeyWord);

	public String getDefaultChannelName();

	public Set<String> getKeyWords();

	public void setKeyWords(Set<String> keyWords);

	public void addKeyWord(String key);

	public void removeKeyWord(String key);

	public void clearKeyWords();

	public boolean isEmptyKeyWords();

	public String parseKeyWords();

	public void parseKeyWords(String filtr);

	public int getNrPage();

	public void setNrPage(int nrPage);

}
