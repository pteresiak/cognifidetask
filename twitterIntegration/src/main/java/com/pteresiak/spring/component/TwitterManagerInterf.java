package com.pteresiak.spring.component;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;

public interface TwitterManagerInterf {

	public Twitter getTwitter();

	public TwitterFactory getTwitterFactory();

	public String getJsonTweets(SearchSettingInterf setting);
}
