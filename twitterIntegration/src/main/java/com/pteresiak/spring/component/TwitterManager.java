package com.pteresiak.spring.component;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import twitter4j.Paging;

import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterObjectFactory;
import twitter4j.User;

@Repository
public class TwitterManager extends TwitterInit implements TwitterManagerInterf {

	public String getJsonTweets(SearchSettingInterf setting) {

		try {

			User user = this.getUser(setting);

			ResponseList<Status> statuses = null;

			if (!setting.isEmptyKeyWords()) {

				statuses = this.twitter.getUserTimeline(user.getScreenName(), new Paging(setting.getNrPage(), 500));

				return this.getFiltredJSON(statuses, setting.getKeyWords());
			} else {
				statuses = this.twitter.getUserTimeline(user.getScreenName(), new Paging(setting.getNrPage(), 50));
				return TwitterObjectFactory.getRawJSON(statuses);
			}

		} catch (TwitterException te) {
			te.printStackTrace();
			System.out.println("Failed to get timeline: " + te.getMessage());
			System.exit(-1);
		}
		return null;

	}

	private User getUser(SearchSettingInterf setting) throws TwitterException {
		User user;
		try {
			user = this.getTwitter().showUser(setting.getChannel());

		} catch (TwitterException twiiterEx) {
			user = this.getTwitter().showUser(setting.getDefaultChannelName());
		}
		return user;
	}

	private boolean filtrTweets(Status status, Set<String> keyWords) {

		for (String string : keyWords) {

			if (!"".equals(string) && '\"' == string.charAt(0) && '\"' == string.charAt(string.length() - 1)) {

				if (searchWord(status, string)) {
					return true;
				}

			} else if (!"".equals(string) && status.getText().toUpperCase().contains(string)) {
				// stringChar
				return true;
			}
		}
		return false;
	}

	private boolean searchWord(Status status, String string) {
		// concret word

		String lookingWord = string.replace("\"", "");
		List<String> statusTxt = Arrays.asList(status.getText().toUpperCase().split(" "));
		return statusTxt.contains(lookingWord);

	}

	private String getFiltredJSON(ResponseList<Status> responseListStatus, Set<String> keyWords) {

		StringBuffer rawJSON = new StringBuffer("[");
		boolean isMinOneTweet = false;
		for (Status status : responseListStatus) {
			if (filtrTweets(status, keyWords)) {
				rawJSON.append(" ").append(TwitterObjectFactory.getRawJSON(status));
				rawJSON.append(",");
				isMinOneTweet = true;
			}
		}
		if (isMinOneTweet) {
			rawJSON.deleteCharAt(rawJSON.length() - 1); // remove last ","
		} else {
			return "[{\"empty\":true}]";
		}

		rawJSON.append("]");

		return rawJSON.toString();
	}
}
