package com.pteresiak.spring.controller;

import java.io.IOException;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.pteresiak.spring.component.SearchSetting;
import com.pteresiak.spring.component.SearchSettingInterf;
import com.pteresiak.spring.component.TwitterManagerInterf;

@RestController
public class HomeController {
	@Autowired
	SearchSettingInterf searchSetting;

	@Autowired
	TwitterManagerInterf twitterManager;

	@Autowired
	HttpServletRequest request;

	@Autowired
	HttpServletResponse response;

	private int cookieMaxAge = 3600 * 24 * 7 * 4; // 4 week

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public void homePage() throws IOException {
		response.sendRedirect("./home");
	};

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView startPage() {

		ModelAndView mvc = new ModelAndView("home");
		mvc.addObject("settings", new SearchSetting());
		String cookieChannelEncode = encodeCookieValue("channel");

		// if not set cookies yet
		if ("".equals(cookieChannelEncode)) {
			cookiesInit();
		}
		return mvc;
	}

	@RequestMapping(value = "/tweets.json", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public String getTimelineFromTwiiter() {

		String cookieChannelEncode = encodeCookieValue("channel");
		String cookieFiltrEncode = encodeCookieValue("filtr");
		String cookieNrPageEncode = encodeCookieValue("nrPage");
		Long nrPage = Long.parseUnsignedLong(cookieNrPageEncode);

		if ("".equals(cookieChannelEncode)) {
			this.searchSetting.setChannel(this.searchSetting.getDefaultChannelName());
			this.searchSetting.clearKeyWords();
			this.searchSetting.setNrPage(1);
		} else {

			this.searchSetting.parseKeyWords(cookieFiltrEncode);
			this.searchSetting.setChannel(cookieChannelEncode);
			this.searchSetting.setNrPage(nrPage.intValue());
		}
		return this.twitterManager.getJsonTweets(this.searchSetting);
	}

	@RequestMapping(value = "/userChannel", method = RequestMethod.POST)
	public void changeChannel(@ModelAttribute SearchSetting newSearchSetting) {
		String newChannel = newSearchSetting.getChannel();
		setCookie(newChannel, "channel");
		this.setCookie("1", "nrPage");
		try {
			response.sendRedirect("./home");
		} catch (IOException e) {
			System.out.println("Error sendRedirect ./home in /userChannel");
			e.printStackTrace();
		}

	}

	@RequestMapping(value = "/nextPage", method = RequestMethod.GET)
	public void nextPage() {
		String cookieNrPageEncode = encodeCookieValue("nrPage");
		Long nrPage = Long.parseUnsignedLong(cookieNrPageEncode);
		nrPage++;

		this.setCookie(nrPage.toString(), "nrPage");

		try {
			response.sendRedirect("./home");
		} catch (IOException e) {
			System.out.println("Error sendRedirect ./home in /userChannel");
			e.printStackTrace();
		}

	}

	@RequestMapping(value = "/prevPage", method = RequestMethod.GET)
	public void prevPage() {
		String cookieNrPageEncode = encodeCookieValue("nrPage");
		Long nrPage = Long.parseUnsignedLong(cookieNrPageEncode);
		if (nrPage > 1) {
			nrPage--;
		} else {
			nrPage = 1L;
		}

		this.setCookie(nrPage.toString(), "nrPage");

		try {
			response.sendRedirect("./home");
		} catch (IOException e) {
			System.out.println("Error sendRedirect ./home in /userChannel");
			e.printStackTrace();
		}

	}

	@RequestMapping(value = "/addKeyWord", method = RequestMethod.POST)
	public void addKeyWord(@ModelAttribute SearchSetting newSearchSetting) {

		this.searchSetting.clearKeyWords();
		this.searchSetting.parseKeyWords(this.encodeCookieValue("filtr"));
		this.searchSetting.addKeyWord(newSearchSetting.getNewKeyWord());
		String cookieFiltr = this.searchSetting.parseKeyWords();
		this.searchSetting.clearKeyWords();
		this.setCookie(cookieFiltr, "filtr");
		this.setCookie("1", "nrPage");
		try {
			response.sendRedirect("./home");
		} catch (IOException e) {
			System.out.println("Error sendRedirect ./home in /addKeyWord");
			e.printStackTrace();
		}
	}

	// NOT USE YET
	@RequestMapping(value = "/removeKeyWord", method = RequestMethod.POST)
	public void removeKeyWord(@ModelAttribute SearchSetting newSearchSetting) {
		this.searchSetting.clearKeyWords();
		this.searchSetting.parseKeyWords(this.encodeCookieValue("filtr"));
		this.searchSetting.removeKeyWord(newSearchSetting.getNewKeyWord());
		String cookieFiltr = this.searchSetting.parseKeyWords();
		this.setCookie(cookieFiltr, "filtr");
		this.setCookie("1", "nrPage");
		try {
			response.sendRedirect("./home");
		} catch (IOException e) {
			System.out.println("Error sendRedirect ./home in /removeKeyWord");
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/clearKeyWords", method = RequestMethod.POST)
	public void removeKeyWord(HttpServletResponse response) {
		// Cookie cookies = new Cookie("filtr", null);
		// cookies.setMaxAge(this.cookieMaxAge);
		// response.addCookie(cookies);
		this.setCookie(null, "filtr");
		this.setCookie("1", "nrPage");
		this.searchSetting.clearKeyWords();
		try {
			response.sendRedirect("./home");
		} catch (IOException e) {
			System.out.println("Error sendRedirect ./home in /clearKeyWord");
			e.printStackTrace();
		}
	}

	private void cookiesInit() {

		setCookie(this.searchSetting.getDefaultChannelName(), "channel");
		setCookie(null, "filtr");
		setCookie("1", "nrPage");
	}

	private String searchCookiesValue(String nameCookie) {
		String value = null;
		Cookie[] cookies = this.request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {

				if (nameCookie.equals(cookie.getName())) {
					value = cookie.getValue();
					return value;
				}
			}
		}
		return value;
	}

	private void setCookie(String value, String name) {

		if (name != null) {
			Cookie cookies;
			if (value != null) {
				String stringUrl = "";
				try {
					stringUrl = URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
				} catch (UnsupportedEncodingException e) {
					stringUrl = "";
					System.out.println("Error URLEncoder " + name + "value: " + value);
					e.printStackTrace();
				}

				cookies = new Cookie(name, stringUrl);
			} else {

				cookies = new Cookie(name, null);
			}
			cookies.setMaxAge(this.cookieMaxAge);
			response.addCookie(cookies);
		} else {
			System.out.println("Error Namme is null");
		}
	}

	private String encodeCookieValue(String cookieName) {
		String cookieValueEncode = "";
		String cookieValue = this.searchCookiesValue(cookieName);
		if (cookieValue != null) {
			try {
				cookieValueEncode = URLDecoder.decode(cookieValue, StandardCharsets.UTF_8.toString());
			} catch (UnsupportedEncodingException e) {
				System.out.println("Error URLDecode " + cookieName + "from " + cookieValue);
				e.printStackTrace();
			}
		}
		return cookieValueEncode;
	}
}
