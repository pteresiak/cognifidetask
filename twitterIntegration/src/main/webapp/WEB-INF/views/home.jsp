<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">


<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="Integration API with Twitter">
<meta name="author" content="Piotr Teresiak">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Twiter TimeLine</title>


<spring:url value="/resources/css/bootstrap.min.css" var="bootstrap_css"></spring:url>
<spring:url value="/resources/css/main.css" var="main_css"></spring:url>
<spring:url value="/resources/js/jquery-3.1.0.min.js" var="jQuery"></spring:url>
<spring:url value="/resources/js/bootstrap.min.js" var="bootstrap_js"></spring:url>
<spring:url value="/resources/js/tweets.js" var="tweets_js"></spring:url>
<spring:url value="/resources/css/normalize.css" var="normalize_css"></spring:url>
<spring:url value="/resources/css/font-awesome.css" var="font_css"></spring:url>

<!-- Bootstrap core CSS -->
<link href="${bootstrap_css}" rel="stylesheet">

<!-- Normalize CSS -->
<link href="${normalize_css}" rel="stylesheet">

<!-- Fonts CSS -->
<link href="${font_css}" rel="stylesheet">


<!-- Main CSS -->
<link href="${main_css}" rel="stylesheet">

<!-- Jquery -->
<script src="${jQuery}"></script>

<!-- tweets time line  js -->
<script src="${tweets_js}"></script>

</head>
<body>
	<div class="master">
		<div class="header">
			<div class="logo col-md-6"></div>
			<div class="options col-md-6">
				<div class="search col-md-6">
					<div class="searchChannel form-inline panel-primary">
						<spring:url value="/userChannel" var="userChannelUrl"></spring:url>
						<form:form action="${userChannelUrl}" method="post"
							modelAttribute="settings">
							<form:input class=" form-control" path="channel" vale="@" />
							<form:button class="button btn-lg btn-primary" type="submit"> Choose</form:button>
						</form:form>
					</div>
					<div class="addedWord form-inline  panel-primary">
						<spring:url value="/addKeyWord" var="addWordlUrl"></spring:url>
						<form:form action="${addWordlUrl}" method="post"
							modelAttribute="settings">
							<form:input class="form-control" path="newKeyWord" />
							<form:button class="button btn-lg btn-primary" type="submit"> Add KeyWord</form:button>
						</form:form>
					</div>
				</div>
		
				<c:if test="${ cookie.filtr.value != '' && cookie.filtr!=null}">

					<div class="filtr form-inline col-md-6  panel-primary">
						<div class="keyWords">
							<h3>Filtry:</h3>
						</div>
						<spring:url value="/clearKeyWords" var="clearFiltr"></spring:url>
						<form:form action="${clearFiltr}" method="post"
							modelAttribute="settings">

							<form:button class="button btn-lg btn-primary" type="submit">Clear Filtr</form:button>
						</form:form>

					</div>
				</c:if>
			</div>
		</div>
		<div class="pageButon">
			<spring:url value="/prevPage" var="prevPageUrl"></spring:url>
			<a href="${prevPageUrl}">
				<button class="button btn-lg btn-primary">Prev Page</button>
			</a>

			<spring:url value="/nextPage" var="nextPageUrl"></spring:url>
			<a href="${nextPageUrl}">
				<button class="button btn-lg btn-primary">Next Page</button>
			</a>

		</div>

		<div class="timeLine">
			<div class="loadingPage"></div>
		</div>
		<div class="pageButon">
			<spring:url value="/prevPage" var="prevPageUrl"></spring:url>
			<a href="${prevPageUrl}">
				<button class="button btn-lg btn-primary">Prev Page</button>
			</a>

			<spring:url value="/nextPage" var="nextPageUrl"></spring:url>
			<a href="${nextPageUrl}">
				<button class="button btn-lg btn-primary">Next Page</button>
			</a>

		</div>

		<div class="footer">
			<p>Created by Piotr Teresiak</p>
		</div>
	</div>
	<!-- Bootstrap JS -->
	<script src="${bootstrap_js}"></script>
</body>
</html>