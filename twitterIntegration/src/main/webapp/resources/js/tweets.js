/**
 * 
 */

$(document)
		.ready(
				function() {

					console.log("Succes");
					$
							.getJSON(
									"tweets.json",
									function(rawJSON) {
										// define function
										var addTweets = function(data) {
											$
													.each(
															data,
															function(key, val) {

																if (typeof val.empty !== "undifined"
																		&& val.empty === true) {

																	var travoltaDiv = $("<div/>");
																	travoltaDiv
																			.addClass("travolta");
																	var emptyTweetDiv = $("<div/>");
																	emptyTweetDiv
																			.addClass(
																					"tweet")
																			.addClass(
																					"panel")
																			.append(
																					"<h1>Empty Tweets</h1>");

																	$(
																			".timeLine")
																			.append(
																					travoltaDiv)
																			.append(
																					emptyTweetDiv);

																} else {
																	$(
																			".timeLine")
																			.append(
																					createDivTweet(val));
																}
															});
											$(".loadingPage").remove();

										};

										var createDivMedia = function(data) {
											var divMedia = $("<div/>");
											divMedia.addClass("media");
											divMedia.addClass("col-md-4");

											if (typeof data.extended_entities !== "undefined") {
												var image = $("<img/>")
												image.addClass("photo");
												image.setAttribute = "class = 'photo'";
												image
														.attr(
																"src",
																data.extended_entities.media[0].media_url_https);
												image.appendTo(divMedia);
											}

											return divMedia;
										}

										var createDivText = function(data) {
											var divTxt = $("<div/>");
											divTxt.addClass("text");
											divTxt.addClass("col-md-4");
											var txt = "<h3>" + data.text
													+ "</h3>";
											divTxt.append(txt);
											return divTxt;
										}

										var createUserInfo = function(data) {
											var divUser = $("<div/>");
											divUser.addClass("userInfo");
											divUser.addClass("col-md-4");

											var info = "<h2>" + "@";
											if (data
													.hasOwnProperty("retweeted_status")) {
												// retweeted
												info += data.retweeted_status.user.screen_name;
											} else {
												info += data.user.screen_name;
											}

											info += "</h2>";
											divUser.append(info);
											return divUser;
										}

										var createDivContentTweet = function(
												data) {
											var divContent = $("<div/>");
											divContent.addClass("contentTweet");
											divContent.addClass("row");
											divContent.addClass("col-md-12");

											divContent
													.append(
															createUserInfo(data))
													.append(createDivText(data))
													.append(
															createDivMedia(data));
											return divContent;
										}

										var createDivDateAdded = function(data) {
											var divDate = $("<div/>");
											divDate.addClass("tweetDate");
											divDate.addClass("col-md-12");
											divDate.addClass("row");

											var dateCreated = "<h2>"
													+ data.created_at + "</h2>";
											// TODO Conversion Date Format
											divDate.append(dateCreated);
											return divDate;

										}

										var createDivTweet = function(data) {
											var divTweet = $("<div/>");
											divTweet.addClass("tweet");
											divTweet.addClass("panel");
											divTweet.addClass("row");
											divTweet
													.append(createDivDateAdded(data));
											divTweet
													.append(createDivContentTweet(data));

											return divTweet;

										}

										function getCookie(cname) {
											var name = cname + "=";
											var ca = document.cookie.split(';');
											for (var i = 0; i < ca.length; i++) {
												var c = ca[i];
												while (c.charAt(0) == ' ') {
													c = c.substring(1);
												}
												if (c.indexOf(name) == 0) {
													return c.substring(
															name.length,
															c.length);
												}
											}
											return "";
										}
										;
										var logoInit = function() {

											var page = getCookie("nrPage");
											var channel = getCookie("channel");

											var string = "<h1>Hello Tweety Page:"
													+ page
													+ "</h1>"
													+ "<h2>Channel @"
													+ channel
													+ "</h2>";

											$(".logo").append(string);

										};

										var addeKetWords = function() {
											var filtr = getCookie("filtr");
											filtr = decodeURIComponent(filtr);
											filtr = filtr.split("+").join(" ");

											$(".keyWords").append(
													"<h4>" + filtr + "</h4>");
										}

										// Create tweet
										// -------------------------------------------------------------------------------
										addTweets(rawJSON);
										logoInit();
										addeKetWords();

									});
				});